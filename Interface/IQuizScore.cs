﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyeGlossary.Interface
{
    abstract class IQuizScore
    {
        // Accessor for the quiz unique ID
        public abstract string UniqueId { get; }

        // Accessor for the Quiz Score, right answers
        public abstract int TotalCorrectAnswers { get; }

        // Accessor for the Quiz score, wrong answers
        public abstract int TotalWrongAnswers { get; }

    }
}
