﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyeGlossary.Interface
{
    abstract class IStudent
    {
        // Accessor for Student FirstName
        public abstract string FirstName { get; }

        // Accessor for Student LastName
        public abstract string LastName { get; }
    }
}
