﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyeGlossary.Interface
{
    abstract class IQuizController
    {
        // Utility method for starting the quiz
        public abstract void Start();

        // Utility method for checking if it is complete
        public abstract bool IsComplete();

        // Utility method for stoping the quiz before it is complete
        public abstract void Stop();

        // Utility method for getting the results of the quiz
        public abstract IQuizScore Results();
    }
}
