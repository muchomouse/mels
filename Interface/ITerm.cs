﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyeGlossary.Interface
{
    abstract class ITerm
    {
        // Accessor for English word
        public abstract string EnglishWord { get; }

        // Accessor for Nowegina word
        public abstract string NorwegianWord { get; }

        public override string ToString()
        {
            return EnglishWord;
        }
    }
}
