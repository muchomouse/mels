My e-Learning Suite 
===================
Latest: v2.0.0
I made this program to help my daughter practice here English/Norwegian Glossary homework. 
Then my wife wanted me to make another program for the Math homework. So I took it one step further
and started on this "Suite" of programs (The maths program hasn't been made yet).

Current features of My Glossary
-------------------------------
- Read Glossary from XML file.
- Bootstrap a sample Glossary XML file if it can't find existing.
- Present random terms from the Glossary to the screen in Norwegian and accept an English answers.
- Count the number of right and wrong answers.
- Flash Console Window in Green, Red or Yellow to indicate Right, Wrong, or Ignored answers.
	- Ignored answers are "null" or "". I added this because the kids kept hitting enter by mistake, and got a lot of wrong answers.
- Logs various things in a logfile.
	- I added this to see what the kids were typing. She kept insisting that she wrote the correct answer but got wrong. So to prove myselfe right I needed a log :)
- Version 1.0.1 or greater will run for at least 20 minutes, and 40 correct answers. But if provided with 60 correct answers, it will stop befor the 20 minutes are up.
- Version 2.0.0 or greater will start by promptiong for pages. This accepts a comma separated list of pages in the XML file. 
  As long at one of the pages exists it will start (ignoring any pages that dont exist). If non of the pages exists it will ask again. 
  It also accepts "all" to use all the pages.

Upcomming/planed features of My Glossary
----------------------------------------
- Read Student XML file.
- Save statistics back to XML file to show student progress.
- Being able to change the timer, and number of required answers in a configuration file or from in the program.
- Probably more...


