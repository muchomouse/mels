﻿using System;

sealed public class TemplateClassWithRegions
{
    // This is a copy/paste Template class
    // I use it for getting my defult #region structure
    #region Members
    // Members go here
    #endregion
    #region Constructor
    // Constructors go here
    #endregion
    #region Helper Methods
    // Helper Methods go here
    #endregion
    #region IParent Overrides
    // Overrides from the parent class go here
    #endregion
    #region IXmlaserialazable Members
    public XmlSchema GetSchema()
    {
        return null;
    }
    public void ReadXml(XmlReader reader)
    {
        bool isEmpty = reader.IsEmptyElement;
        reader.ReadStartElement(); // <Student>
        if (isEmpty) return;
        while (reader.NodeType == XmlNodeType.Element)
        {
        }
        reader.ReadEndElement(); // </Student>
    }
    public void WriteXml(XmlWriter writer)
    {
    }

    #endregion

}
