﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using MyeGlossary.Glossary_Book;
using log4net;
using System.IO;

namespace EngelskeGlosser
{
    class Program
    {
        static void Main(string[] args)
        {
            // Branding :P
            Console.Title = "My e-Glossary v2.1";
            ConsoleHelper.SetConsoleFont(20);

            //Read log4net config, loggers within each class  
            string logConfigFilePath = AppDomain.CurrentDomain.BaseDirectory + "\\MyeGlossary.log4net";
            log4net.Config.XmlConfigurator.Configure(new FileInfo(logConfigFilePath));
            
            // This within each class that uses the logger
            ILog myLog = LogManager.GetLogger(typeof(Program));

            //StudentGroup myStudentGroup = new StudentGroup();
            //myStudentGroup.Interactive();
            
            // Make the Quizzer
            GlossaryQuiz myQuizer = new GlossaryQuiz(20, 40, 60);
            
            // Propt for page input
            string pages = "all";
            while (true)
            {
                
                Console.Write("Velg sider. > ");
                pages = Console.ReadLine();
                Console.Clear();
                if (myQuizer.SelectPages(pages))
                    break;
                else
                    Console.WriteLine("Ingen gyldige sider, prøv igjen eller skriv \"all\" for å velge alle sider.");
            }
            //Feed pages to the glossary

            myQuizer.Start();
        }
    }
}
