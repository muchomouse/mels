﻿using MyeGlossary.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace MyeGlossary.Glossary_Book
{
    class Student : IStudent, IXmlSerializable
    {
        #region Members
        // Private Members
        string mFirstName;
        string mLastName;
        int mUniqueID;

        // Public Accessors
        public override string FirstName { get { return mFirstName; } }
        public override string LastName { get { return mLastName; } }
        public string FullName { get { return mLastName + ", " + mFirstName; } }
        public int UniqueId { get { return mUniqueID; } }
        #endregion
        #region Constructor
        public Student(string FirstName, string LastName, int UniqueID)
        {
            mFirstName = FirstName;
            mLastName = LastName;
            mUniqueID = UniqueID;
        }
        public Student(XmlReader reader)
        {
            ReadXml(reader);
        }
        #endregion
        #region Helper methods
        #endregion
        #region IXmlaserialazable Members
        public XmlSchema GetSchema()
        {
            return null;
        }
        public void ReadXml(XmlReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            if (isEmpty) return;
            mUniqueID = int.Parse(reader.GetAttribute("UniqueID"));
            reader.ReadStartElement();
            mFirstName = reader.ReadElementContentAsString();
            mLastName = reader.ReadElementContentAsString();
            reader.ReadEndElement(); // </Student>
        }
        public void WriteXml(XmlWriter writer)
        {
            writer.WriteElementString("FirstName", mFirstName);
            writer.WriteElementString("LastName", mLastName);
        }
        #endregion
    }
}
