﻿using MyeGlossary.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Schema;
using log4net;

namespace MyeGlossary.Glossary_Book

{
    class Glossary : IGlossary 
    {
        #region Members
        // This within each class that uses the logger
        ILog myLog = LogManager.GetLogger(typeof(Glossary));

        List<Term> mGlossary = new List<Term>();
        List<Term> mSelectedTerms = new List<Term>();

        public List<Term> GlossaryBook { get { return mGlossary; } }

        Random mRandom = new Random(DateTime.Now.Second);
        #endregion
        #region Constructor
        public Glossary()
        {
            try
            {
                ImportFromXML();
            }
            catch (Exception)
            {   
                mGlossary.Clear();
            }

            if (mGlossary.Count == 0)
            {
                GenerateSampleGlossary();
                mGlossary.Clear();
                ImportFromXML();
            }
        }
        #endregion
        #region Helper Methods
        public bool AddTerm(string English, string Norwegian, int Page)
        {
            mGlossary.Add(new Term(English, Norwegian,Page));
            return false;
        }
        public void ExportToXML()
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;
            settings.NewLineHandling = NewLineHandling.Entitize;

            XmlWriter writer = XmlWriter.Create("Glossary.xml", settings);
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Glossary");
                WriteXml(writer);
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
            writer.Close();
        }
        public void ImportFromXML()
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            XmlReader reader = XmlReader.Create("Glossary.xml", settings);
            ReadXml(reader);
            reader.Close();
        }
        public void GenerateSampleGlossary()
        {
            mGlossary.Add(new Term("a mess", "rot", 7));
            mGlossary.Add(new Term("paint", "måle", 7));
            mGlossary.Add(new Term("buy", "kjøpe", 7));
            mGlossary.Add(new Term("ready", "klar", 7));
            mGlossary.Add(new Term("new", "ny", 7));
            mGlossary.Add(new Term("cooking", "lage mat", 7));
            mGlossary.Add(new Term("talk about", "snakke om", 6));
            mGlossary.Add(new Term("floors", "etasjar", 6));
            mGlossary.Add(new Term("outside", "utanfor", 6));
            mGlossary.Add(new Term("tell me", "fortel meg", 6));
            mGlossary.Add(new Term("flat", "leilighet", 6));
            mGlossary.Add(new Term("small", "liten", 6));

            ExportToXML();
        }
        public int GetLastPage()
        {
            int latestPage = -1;
            foreach (Term term in mGlossary)
	        {
		        if(latestPage < term.Page)
                    latestPage = term.Page;
	        }
            return latestPage;
        }
        public int GetFirstPage()
        {
            int firstPage = 999999999;
            foreach (Term term in mGlossary)
            {
                if (firstPage > term.Page)
                    firstPage = term.Page;
            }
            return firstPage;

        }
        public bool MakeListOfRandomTerms(string listOfPages)
        {
            List<Term> results = new List<Term>();

            if (listOfPages != "all")
            {
                string[] pageArray = listOfPages.Split(',');
                foreach (Term term in mGlossary)
                {
                    bool isSelected = false;
                    for (int i = 0; i < pageArray.Length; i++)
                    {
                        if (term.Page.ToString() == pageArray[i])
                            isSelected = true;
                    }
                    if (isSelected)
                        results.Add(term);
                }
            }
            else
            {
                foreach (Term term in mGlossary)
                {
                    results.Add(term);
                }
            }
            if (results.Count() > 0)
            {
                mSelectedTerms = results;
                return true;
            }
            return false;
        }
        public Term GetRandomTerm()
        {
            int aRandomIndex = mRandom.Next(mSelectedTerms.Count());
            return mSelectedTerms[aRandomIndex];
        }
        #endregion
        #region IGlossary Overrides
        #endregion
        #region IXmlaserialazable Members
        public XmlSchema GetSchema()
        {
            return null;
        }
        public void ReadXml(XmlReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement(); // <Glossary>
            if (isEmpty) return;
            int newPage = -1;
            while (reader.NodeType == XmlNodeType.Element)
            {
                if (reader.Name == "Page")
                {
                    newPage = int.Parse(reader.GetAttribute("nr"));
                }
                else if (reader.Name == "Term")
                {
                    GlossaryBook.Add(new Term(reader, newPage));
                }
                else
                    throw new XmlException("Unexpected node: " + reader.Name);
                
                reader.ReadStartElement();
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name != "Glossary")
                {
                    reader.ReadEndElement();
                }
            }
            reader.ReadEndElement(); // </Glossary>
        }
        public void WriteXml(XmlWriter writer)
        {
            int currentPage = -1;
            foreach (Term term in GlossaryBook)
            {
                if (currentPage != term.Page && currentPage != -1)
                {
                    writer.WriteEndElement();
                    writer.WriteStartElement("Page");
                    writer.WriteAttributeString("nr", term.Page.ToString());
                }
                else if (currentPage == -1)
                {
                    writer.WriteStartElement("Page");
                    writer.WriteAttributeString("nr", term.Page.ToString());
                }
                currentPage = term.Page;
                term.WriteXml(writer);
            }
            writer.WriteEndElement();
        }

        #endregion
    }
}
