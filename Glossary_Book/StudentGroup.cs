﻿using MyeGlossary.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace MyeGlossary.Glossary_Book
{
    class StudentGroup : IStudentGroup, IXmlSerializable
    {
        #region Members
        List<Student> mStudents = new List<Student>();
        public List<Student> Students { get { return mStudents; } }

        Student mCurrentStudent;
        #endregion
        #region Contructors
        public StudentGroup()
        {
            try
            {
                ImportFromXML();
            }
            catch (Exception)
            {
                mStudents.Clear();
            }
            if (mStudents.Count == 0)
            {
                BootStrapSampleStudents();
                mStudents.Clear();
                ImportFromXML();
            }
        }
        #endregion
        #region Helper Methods
        public void Interactive()
        {
            Console.WriteLine("Velkommen til Glose øving.");
            Console.WriteLine("Før me begynner må du fortele meg kven du er:");
            Console.WriteLine("1. Registrer ny elev.");
            Console.WriteLine("2. Velg registrert elev.");
            Console.Write("> ");
            string userInput = Console.ReadLine();
            switch (userInput)
            {
                case "1":
                    Console.Clear();
                    AddNewStudent();
                    break;
                case "2":
                    Console.Clear();
                    mCurrentStudent = mStudents[SelectSavedStudents()];
                    break;
                default:
                    break;
            }
            Console.Clear();
            Console.WriteLine("Velkommen {0}. Klar for Glose Øving?",mCurrentStudent.FirstName);
            Console.WriteLine("Trykk Enter for å starte");
            Console.ReadLine();
        }
        private int SelectSavedStudents()
        {
            int rUniqueID = -1;
            int i=0;
            foreach (Student s in mStudents)
            {
                Console.WriteLine("{0}: {1}",i,s.FullName);
                i++;
            }
            string userInput = Console.ReadLine();
            rUniqueID = mStudents[int.Parse(userInput)].UniqueId;
            return rUniqueID;
        }
        private void AddNewStudent()
        {
            Console.Write("Fornamn: ");
            string rFirstName = Console.ReadLine();
            Console.Write("Etternamn: ");
            string rLastName = Console.ReadLine();
            AddNewStudent(rFirstName, rLastName);
            Console.WriteLine("Laget ny elev med ID {0}", mStudents.Last().UniqueId);
            Console.ReadLine();
            mCurrentStudent = mStudents.Last();
            ExportToXML();
        }
        private void AddNewStudent(string FirstName, string LastName)
        {
            int uKey = mStudents.Count();
            mStudents.Add(new Student(FirstName,LastName,uKey));
        }
        public void BootStrapSampleStudents()
        {
            mStudents.Clear();
            mStudents.Add(new Student("Ola", "Nordman",0));
            mStudents.Add(new Student("Kari", "Nordman",1));
            ExportToXML();
        }
        public void ExportToXML()
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;
            settings.NewLineHandling = NewLineHandling.Entitize;

            XmlWriter writer = XmlWriter.Create("Students.xml", settings);
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Students");
                WriteXml(writer);
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
            writer.Close();
        }
        public void ImportFromXML()
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.IgnoreWhitespace = true;
            XmlReader reader = XmlReader.Create("Students.xml", settings);
            ReadXml(reader);
            reader.Close();
        }
        #endregion
        #region IXmlaserialazable Members
        public XmlSchema GetSchema()
        {
            return null;
        }
        public void ReadXml(XmlReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement(); // <Students>
            if (isEmpty) return;
            while (reader.NodeType == XmlNodeType.Element)
            {
                if (reader.Name == "Student")
                {
                    mStudents.Add(new Student(reader));
                }
                else
                    throw new XmlException("Unexpected node: " + reader.Name);

            }
            reader.ReadEndElement(); // </Students>
        }
        public void WriteXml(XmlWriter writer)
        {
            foreach (Student student in mStudents)
            {
                writer.WriteStartElement("Student");
                writer.WriteAttributeString("UniqueID", student.UniqueId.ToString());
                student.WriteXml(writer);
                writer.WriteEndElement();
            }

        }
        #endregion
    }
}
