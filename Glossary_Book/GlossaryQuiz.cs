﻿using log4net;
using MyeGlossary.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace MyeGlossary.Glossary_Book
{
    class GlossaryQuiz : IQuizController
    {
        #region Members
        // This within each class that uses the logger
        ILog myLog = LogManager.GetLogger(typeof(GlossaryQuiz));

        DateTime mFinished;
        int mCorrectAnswers = 0;
        int mWrongAnswers = 0;
        int mQuizMinutes = 20;
        int mQuizScore = 30;
        int mEarlyScore = 100;
        string mPages = "all";
        bool mEndedEarly = false;

        // Make the glossary
        Glossary mGlossary = new Glossary();

        #endregion
        #region Constructors
        /// <summary>
        /// Makes a GlossaryQuiz with a 20 minute timer, and requires 60 correct answers to complete. Terminates early with 120 correct answers.
        /// </summary>
        public GlossaryQuiz()
        {
            // This does nothing.
        }
        /// <summary>
        /// Make a GlossaryQuiz with the specified parameters
        /// </summary>
        /// <param name="QuizMinutes">The number of minutes to keep quizing</param>
        /// <param name="QuizScore">The number of correct anwers required to end the quiz, once the timer runs out.</param>
        /// <param name="EarlyScore">The number of correct answers that will end the quiz before the timer runs out. Will not be evaluated if 0.</param>
        public GlossaryQuiz(int QuizMinutes, int QuizScore, int EarlyScore)
        {
            mQuizMinutes = QuizMinutes;
            mQuizScore = QuizScore;
            mEarlyScore = EarlyScore;
        }
        #endregion
        #region Helper methods
        //TODO: Add helper methods
        #endregion
        #region IQuizController overrides
        public override void Start()
        {
            mFinished = DateTime.Now.AddMinutes(mQuizMinutes);
            myLog.InfoFormat("Staring new quiz to end in {0} minutes, with {1} correct answers and an earlyEnding score of {2}",mQuizMinutes,mQuizScore,mEarlyScore);          
            // Change the Console Look and feel
            Console.Title = "Engelsk Gloser";

            string lastWord = null;
            while (!IsComplete() || mCorrectAnswers < mQuizScore)
            {
                // Clean up the Colors and text
                Console.ResetColor();
                Console.Clear();

                // get an random term
                Term thisTerm = mGlossary.GetRandomTerm();
                if (lastWord != thisTerm.ToString())
                {
                    Console.WriteLine("Norsk: " + thisTerm.NorwegianWord);
                    Console.Write("Engelsk: ");
                    string userAnswer = Console.ReadLine();

                    if (userAnswer == thisTerm.EnglishWord)
                    {
                        lastWord = thisTerm.ToString(); // we don't ask this again for the next question
                        mCorrectAnswers++;
                        myLog.InfoFormat("CORRECT: \"{0}\" is \"{1}\"",thisTerm.NorwegianWord,userAnswer);
                        Console.BackgroundColor = ConsoleColor.Green;
                        Console.Clear();
                    }
                    else if(userAnswer == null || userAnswer == "")
                    {
                        myLog.DebugFormat("BLANK:  \"{0}\" is not \"{1}\"", thisTerm.NorwegianWord, userAnswer);
                        Console.BackgroundColor = ConsoleColor.Yellow;
                        Console.Clear();
                    }
                    else
                    {
                        mWrongAnswers++;
                        myLog.InfoFormat("WRONG: \"{0}\" is not \"{1}\"", thisTerm.NorwegianWord, userAnswer);
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.Clear();
                        Console.Beep();
                    }
                }
                Thread.Sleep(1000);
                // Break for early ending
                if (mEarlyScore != 0 && mEarlyScore < mCorrectAnswers)
                {
                    mEndedEarly = true;
                    break;
                }
            }
            Console.ResetColor();
            Console.Clear();
            if(mEndedEarly)
            {
                Console.WriteLine("Du er kjempe flink!");
                myLog.Info("Quiz ended early.");
            }
            else
            {
                Console.WriteLine("Du er Ferdig!");
            }
            myLog.InfoFormat("Quiz ended with {0} correct vs {1} wrong", mCorrectAnswers, mWrongAnswers);
            Console.WriteLine("Du hadde {0} rette svar og {1} feil.", mCorrectAnswers, mWrongAnswers);
            Console.ReadLine();

        }
        public override void Stop()
        {
            throw new NotImplementedException();
        }
        public override bool IsComplete()
        {
            if (DateTime.Now < mFinished)
                return false;
            else
                return true;
        }
        public override IQuizScore Results()
        {
            throw new NotImplementedException();
        }
        public bool SelectPages(string pages)
        {
            return mGlossary.MakeListOfRandomTerms(pages);
        }
        #endregion
    }
}
