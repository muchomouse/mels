﻿using MyeGlossary.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace MyeGlossary.Glossary_Book
{
    class GlossaryScore : IQuizScore, IXmlSerializable 
    {
        #region Members
        // Private Members
        //string mUniqueID;
        //int mTotalCorrectAnswers;
        //int mTotalWrongAnswers;

        // Public Accessors
        public override string UniqueId
        {
            get { throw new NotImplementedException(); }
        }
        public override int TotalCorrectAnswers
        {
            get { throw new NotImplementedException(); }
        }
        public override int TotalWrongAnswers
        {
            get { throw new NotImplementedException(); }
        }
        #endregion
        #region Constructor
        // Constructors go here
        #endregion
        #region Helper Methods
        // Helper Methods go here
        #endregion
        #region IQuizScore Overrides
        // Overrides from the parent class go here
        #endregion
        #region IXmlaserialazable Members
        public XmlSchema GetSchema()
        {
            return null;
        }
        public void ReadXml(XmlReader reader)
        {
            bool isEmpty = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (isEmpty) return;
            while (reader.NodeType == XmlNodeType.Element)
            {
            }
            reader.ReadEndElement();
        }
        public void WriteXml(XmlWriter writer)
        {
        }

        #endregion
    }
}
