﻿using MyeGlossary.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Schema;

namespace MyeGlossary.Glossary_Book
{
    class Term : ITerm
    {
        #region Members
        // The English word(s)
        string mEnglishWord;
        public override string EnglishWord { get { return mEnglishWord; } }
        
        // The corresponding Norwegian words
        string mNorwegianWord;
        public override string NorwegianWord { get { return mNorwegianWord; } }

        // The page in your Glossary
        int mPage;
        public int Page { get { return mPage; } }
        #endregion
        #region Constructor
        public Term()
        {
            // do stuff
        }
        public Term(string English, string Norwegian, int page)
        {
            mEnglishWord = English;
            mNorwegianWord = Norwegian;
            mPage = page;
        }
        public Term(XmlReader reader, int Page)
        {
            mPage = Page;
            ReadXml(reader);
        }
        #endregion
        #region IXmlaserialazable Members
        public XmlSchema GetSchema()
        {
            return null;
        }
        public void ReadXml(XmlReader reader)
        {
            if (reader.Name == "Term" && reader.NodeType == XmlNodeType.Element)
            {
                mEnglishWord = reader.GetAttribute("english");
                mNorwegianWord = reader.GetAttribute("norwegian");
            }
        }
        public void WriteXml(XmlWriter writer)
        {
                writer.WriteStartElement("Term");
                writer.WriteAttributeString("english", mEnglishWord);
                writer.WriteAttributeString("norwegian", mNorwegianWord);
                writer.WriteEndElement();
        }
        #endregion
    }
}
